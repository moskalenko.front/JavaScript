class Coffee {
    cost() {
        return 0;
    }
}
const sizeS = coffee => {
    const cost = coffee.cost();
    coffee.cost = () => cost + 3;
}
const sizeM = coffee => {
    const cost = coffee.cost();
    coffee.cost = () => cost + 5;
}
const sizeL = coffee => {
    const cost = coffee.cost();
    coffee.cost = () => cost + 7;
}
const sugar = coffee => {
    const cost = coffee.cost();
    coffee.cost = () => cost + 1;
}
const milk = coffee => {
    const cost = coffee.cost();
    coffee.cost = () => cost + 2;
}

const coffee = new Coffee();
sizeS(coffee);
sugar(coffee);
milk(coffee)
console.log(coffee.cost());
