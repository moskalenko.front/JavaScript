let counterMod = (function() {
   let counter = 0,
       instance;
   
   let createInstance = () => {
      return {
         getCounter: getCounter,
         incrementCounter: incrementCounter
      }
   }

   let getCounter = () => {
      return console.log(counter);
   }

   let incrementCounter = () => {
      counter++;
   }

   return {
      getInstance: () => {
         return instance || (instance = createInstance())
      }
   }
})();

let instance = counterMod.getInstance();

instance1.incrementCounter();
instance1.getCounter();
