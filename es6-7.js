//Arrow Functions
(function (){
   let f1 = () => console.log("Arrow Functions");
   let f2 = () => {
      const A = 1;
      const B = 2
      return A + B;
   };
   let f3 = param => param;
   let obj = {
      arrowFunctions: (...args) => console.log(this, args),
      f: function () { return console.log(this, arguments)} 
   }
   // obj.arrowFunctions(1, true);
   obj.f(2, false);
})();
//

//destructuring
(function () {
   let {width, height} = {
      width: 100,
      height: 150
   } 
   console.log(width, height)

   function generateObj () {
      return {
         a: 200,
         b: 200
      }
   }
   let {a: newA, b: newB, d = 20} = generateObj();
   console.log(newA, newB, d);

   let [one, two, three = 3] = [1,2]
   console.log(one, two, three)
})();
//

//modules
(function () {
   // import {sum as sum1} from './module'
   // console.log(sum1);
})();
//

//Template literals
(function () {
   let name = "Andrey",
       years = 22;
   
   const tag = (strings, ...values) => {
      console.log(strings, values);
   }

   let message = tag`My name is ${name}. I am ${years} years old`;
})();
//

//Class
(function () {
   class Human {
      constructor(name, age, city) {
         this.name = name;
         this.age = age;
         this.city = city;
      }

      info () {
         console.log(`Name: ${this.name}\nAge: ${this.age}\nCity: ${this.city}`)
      }
   }

   class NLO extends Human{
      constructor(name, age, city) {
         super(name, age, city)
      }

      static hello() {
         console.log('Hello people')
      }
   }

   let andrey = new Human('Andrey', 22, "Kiev");
   let yoda = new NLO('Yoda', 5233, "Mars");

   NLO.hello();
   andrey.info();
   yoda.info();

})();
//

//promises
(function () {
   const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
         if(true) {
            resolve("Hello word");
         } else {
            reject("error");
         }
      }, 2000)
   });

   promise
      .then(data => {
         console.log("success1:", data)
         return data + ' ' + data
      }) .then (data => console.log("success2:", data))
      .catch(error => console.log("error:", error))
})();
//

//async await
(function () {
   const fetchProducts = new Promise((resolve, reject) => {
      setTimeout(() => {
         if(true) {
            resolve([1,2,3,4,5]);
         } else {
            reject("error");
         }
      }, 2000)
   });

   let getProducts = async () => {
      try{   
         let products = await fetchProducts;
         console.log(products);
         console.log('end')
      } catch (err) {
         console.log(err)
      }
   }
   getProducts();
})();
//